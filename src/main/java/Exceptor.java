import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.ClassReader;
import java.util.zip.ZipEntry;
import java.io.ByteArrayOutputStream;
import java.util.zip.ZipOutputStream;
import java.util.zip.ZipInputStream;
import java.io.OutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.File;
import java.io.Reader;
import java.io.InputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.Properties;
import java.util.logging.Logger;

public class Exceptor
{
    private static final Logger log;
    public final Properties mappings;
    
    static {
        log = Logger.getLogger("Exceptor");
    }
    
    public static void main(final String[] args) {
        if (args.length < 4) {
            System.out.println("Exceptor [IN] [OUT] [MAPFILE] [LOGFILE]");
            System.exit(1);
        }
        final Formatter formatter = new ExceptorFormatter();
        log.setUseParentHandlers(false);
        log.setLevel(Level.FINE);
        try {
            final FileHandler filehandler = new FileHandler(args[3], false);
            filehandler.setFormatter(formatter);
            log.addHandler(filehandler);
        }
        catch (Exception exception) {
            System.out.println("Could not create logfile");
            System.exit(1);
        }
        System.out.println("Exceptor v1.0 by Searge");
        log.log(Level.INFO, "Exceptor v1.0 by Searge");
        log.log(Level.INFO, "Input: " + args[0]);
        log.log(Level.INFO, "Output: " + args[1]);
        log.log(Level.INFO, "Mappings: " + args[2]);
        final Exceptor exc = new Exceptor();
        if (!exc.processJar(args[0], args[1], args[2])) {
            System.out.println("Error processing the jar");
            System.exit(1);
        }
        System.out.println("Processed " + args[0]);
    }
    
    public Exceptor() {
        mappings = new Properties();
    }
    
    public boolean loadMappings(final String fileName) {
        InputStream instream = null;
        Reader reader = null;
        try {
            instream = new FileInputStream(fileName);
            reader = new InputStreamReader(instream);
            mappings.load(reader);
        }
        catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        finally {
            try {
                if (reader != null) {
                    reader.close();
                }
                if (instream != null) {
                    instream.close();
                }
            }
            catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        try {
            if (reader != null) {
                reader.close();
            }
            if (instream != null) {
                instream.close();
            }
        }
        catch (Exception e2) {
            e2.printStackTrace();
        }
        return true;
    }
    
    public boolean processJar(final String inFileName, final String outFileName, final String configFile) {
        if (!this.loadMappings(configFile)) {
            System.out.println("Can't load mappings");
            return false;
        }
        final File inFile = new File(inFileName);
        final File outFile = new File(outFileName);
        if (!inFile.isFile()) {
            System.out.println("Can't find input file");
            return false;
        }
        OutputStream outStream;
        try {
            outStream = new FileOutputStream(outFile);
        }
        catch (FileNotFoundException e1) {
            e1.printStackTrace();
            return false;
        }
        InputStream inStream;
        try {
            inStream = new FileInputStream(inFile);
        }
        catch (FileNotFoundException e3) {
            try {
                outStream.close();
                outFile.delete();
            }
            catch (IOException e2) {
                e2.printStackTrace();
            }
            e3.printStackTrace();
            return false;
        }
        final boolean result = this.processJar(inStream, outStream);
        try {
            outStream.close();
        }
        catch (IOException e4) {
            e4.printStackTrace();
            return false;
        }
        finally {
            try {
                inStream.close();
            }
            catch (IOException e5) {
                e5.printStackTrace();
                return false;
            }
        }
        try {
            inStream.close();
        }
        catch (IOException e5) {
            e5.printStackTrace();
            return false;
        }
        return result;
    }
    
    public boolean processJar(final InputStream inStream, final OutputStream outStream) {
        final ZipInputStream inJar = new ZipInputStream(inStream);
        final ZipOutputStream outJar = new ZipOutputStream(outStream);
        boolean reading = true;
        while (reading) {
            ZipEntry entry;
            try {
                entry = inJar.getNextEntry();
            }
            catch (IOException e) {
                System.out.println("Could not get entry");
                return false;
            }
            if (entry == null) {
                reading = false;
            }
            else {
                if (entry.isDirectory()) {
                    try {
                        outJar.putNextEntry(entry);
                        continue;
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                        return false;
                    }
                }
                final byte[] data = new byte[4096];
                final ByteArrayOutputStream entryBuffer = new ByteArrayOutputStream();
                try {
                    int len;
                    do {
                        len = inJar.read(data);
                        if (len > 0) {
                            entryBuffer.write(data, 0, len);
                        }
                    } while (len != -1);
                }
                catch (IOException e2) {
                    e2.printStackTrace();
                    continue;
                }
                byte[] entryData = entryBuffer.toByteArray();
                final String entryName = entry.getName();
                log.log(Level.INFO, "Processing " + entryName);
                if (entryName.endsWith(".class")) {
                    entryData = this.process(entryData);
                }
                log.log(Level.INFO, "Processed " + entryBuffer.size() + " -> " + entryData.length);
                try {
                    final ZipEntry newEntry = new ZipEntry(entryName);
                    outJar.putNextEntry(newEntry);
                }
                catch (IOException e3) {
                    e3.printStackTrace();
                    return false;
                }
                try {
                    outJar.write(entryData);
                }
                catch (IOException e3) {
                    e3.printStackTrace();
                    return false;
                }
            }
        }
        try {
            outJar.close();
        }
        catch (IOException e4) {
            e4.printStackTrace();
            return false;
        }
        finally {
            try {
                inJar.close();
            }
            catch (IOException e5) {
                e5.printStackTrace();
                return false;
            }
        }
        try {
            inJar.close();
        }
        catch (IOException e5) {
            e5.printStackTrace();
            return false;
        }
        return true;
    }
    
    public byte[] process(final byte[] cls) {
        final ClassReader cr = new ClassReader(cls);
        final ClassWriter cw = new ClassWriter(1);
        final ExceptorClassAdapter ca = new ExceptorClassAdapter(cw, this);
        cr.accept(ca, 0);
        return cw.toByteArray();
    }
}
