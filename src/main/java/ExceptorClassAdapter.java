import org.objectweb.asm.MethodVisitor;
import java.util.logging.Level;
import org.objectweb.asm.ClassVisitor;
import java.util.logging.Logger;
import org.objectweb.asm.ClassAdapter;

public class ExceptorClassAdapter extends ClassAdapter
{
    private static final Logger log;
    private Exceptor exc;
    String className;
    
    static {
        log = Logger.getLogger("Exceptor");
    }
    
    public ExceptorClassAdapter(final ClassVisitor cv, final Exceptor exc) {
        super(cv);
        this.exc = exc;
    }
    
    @Override
    public void visit(final int version, final int access, final String name, final String signature, final String superName, final String[] interfaces) {
        log.log(Level.FINE, "Class: " + name + " Extends: " + superName);
        super.visit(version, access, className = name, signature, superName, interfaces);
    }
    
    @Override
    public MethodVisitor visitMethod(final int access, final String name, final String desc, final String signature, String[] exceptions) {
        log.log(Level.FINER, "Name: " + name + " Desc: " + desc + " Sig: " + signature);
        final String clsSig = String.valueOf(className) + "." + name + desc;
        if (exceptions != null && exceptions.length > 0) {
            String exceptionList = "=";
            String[] array;
            for (int length = (array = exceptions).length, i = 0; i < length; ++i) {
                final String exception = array[i];
                if (exceptionList.equals("=")) {
                    exceptionList = String.valueOf(exceptionList) + exception;
                }
                else {
                    exceptionList = String.valueOf(exceptionList) + "," + exception;
                }
            }
            log.log(Level.FINEST, String.valueOf(clsSig) + exceptionList);
        }
        final String excList = exc.mappings.getProperty(clsSig);
        if (excList != null) {
            log.log(Level.FINE, "Adding Exceptions: " + excList + " to " + clsSig);
            exceptions = this.getExceptions(excList);
        }
        final MethodVisitor mv = this.cv.visitMethod(access, name, desc, signature, exceptions);
        return mv;
    }
    
    private String[] getExceptions(final String exceptionList) {
        return exceptionList.split(",");
    }
}
